package by.epam.task.service;

import by.epam.task.domain.Project;

/**
 * Created by Pavel_Mikhailiuk on 11/3/2016.
 */
public interface ProjectService extends CRUDService<Project> {
    boolean assignToProject(Long projectId, Long employeeId);
}
