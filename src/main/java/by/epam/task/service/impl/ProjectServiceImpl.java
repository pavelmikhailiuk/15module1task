package by.epam.task.service.impl;

import by.epam.task.domain.Employee;
import by.epam.task.domain.Project;
import by.epam.task.repository.EmployeeRepository;
import by.epam.task.repository.ProjectRepository;
import by.epam.task.service.ProjectService;
import by.epam.task.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Pavel_Mikhailiuk on 10/27/2016.
 */
@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public void delete(Long id) throws ServiceException {
        if (id == null) {
            throw new ServiceException("Id is null");
        }
        try {
            projectRepository.delete(id);
        } catch (Exception e) {
            throw new ServiceException("Error delete project", e);
        }
    }

    @Override
    public Project update(Project entity) throws ServiceException {
        if (entity == null) {
            throw new ServiceException("project is null");
        }
        Project project = null;
        try {
            project = projectRepository.save(entity);
        } catch (Exception e) {
            throw new ServiceException("Error update project", e);
        }
        return project;
    }

    @Override
    public Long save(Project entity) throws ServiceException {
        if (entity == null) {
            throw new ServiceException("Project is null");
        }
        Long id = null;
        try {
            Project project = projectRepository.save(entity);
            id = project != null ? project.getId() : null;
        } catch (Exception e) {
            throw new ServiceException("Error save project", e);
        }
        return id;
    }

    @Override
    public Project find(Long id) throws ServiceException {
        if (id == null) {
            throw new ServiceException("Id is null");
        }
        Project project = null;
        try {
            project = projectRepository.findOne(id);
        } catch (Exception e) {
            throw new ServiceException("Error find project", e);
        }
        return project;
    }

    @Override
    public boolean assignToProject(Long projectId, Long employeeId) {
        boolean success = false;
        if (projectId == null || employeeId == null) {
            throw new ServiceException("Project or employee id is null");
        }
        Project project = null;
        Employee employee = null;
        try {
            project = projectRepository.findOne(projectId);
            employee = employeeRepository.findOne(employeeId);
            if (project != null && employee != null) {
                project.getEmployees().add(employee);
                success = true;
            } else {
                throw new ServiceException("project or employee not found");
            }

        } catch (Exception e) {
            throw new ServiceException("Error assign employee to project", e);
        }
        return success;
    }
}
