package by.epam.task.service.impl;

import by.epam.task.domain.Employee;
import by.epam.task.repository.EmployeeRepository;
import by.epam.task.service.EmployeeService;
import by.epam.task.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Pavel_Mikhailiuk on 10/27/2016.
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public void delete(Long id) throws ServiceException {
        if (id == null) {
            throw new ServiceException("Id is null");
        }
        try {
            employeeRepository.delete(id);
        } catch (Exception e) {
            throw new ServiceException("Error delete employee", e);
        }
    }

    @Override
    public Employee update(Employee entity) throws ServiceException {
        if (entity == null) {
            throw new ServiceException("Employee is null");
        }
        Employee employee = null;
        try {
            employee = employeeRepository.save(entity);
        } catch (Exception e) {
            throw new ServiceException("Error update employee", e);
        }
        return employee;
    }

    @Override
    public Long save(Employee entity) throws ServiceException {
        if (entity == null) {
            throw new ServiceException("Employee is null");
        }
        Long id = null;
        try {
            Employee employee = employeeRepository.save(entity);
            id = employee != null ? employee.getId() : null;
        } catch (Exception e) {
            throw new ServiceException("Error save employee", e);
        }
        return id;
    }

    @Override
    public Employee find(Long id) throws ServiceException {
        if (id == null) {
            throw new ServiceException("Id is null");
        }
        Employee employee = null;
        try {
            employee = employeeRepository.findOne(id);
        } catch (Exception e) {
            throw new ServiceException("Error find employee", e);
        }
        return employee;
    }
}
