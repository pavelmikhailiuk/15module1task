package by.epam.task.service.impl;

import by.epam.task.domain.Employee;
import by.epam.task.domain.Project;
import by.epam.task.domain.Unit;
import by.epam.task.repository.EmployeeRepository;
import by.epam.task.repository.UnitRepository;
import by.epam.task.service.UnitService;
import by.epam.task.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Pavel_Mikhailiuk on 10/27/2016.
 */
@Service
public class UnitServiceImpl implements UnitService {

    @Autowired
    private UnitRepository unitRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public void delete(Long id) throws ServiceException {
        if (id == null) {
            throw new ServiceException("Id is null");
        }
        try {
            unitRepository.delete(id);
        } catch (Exception e) {
            throw new ServiceException("Error delete unit", e);
        }
    }

    @Override
    public Unit update(Unit entity) throws ServiceException {
        if (entity == null) {
            throw new ServiceException("Unit is null");
        }
        Unit unit = null;
        try {
            unit = unitRepository.save(entity);
        } catch (Exception e) {
            throw new ServiceException("Error update unit", e);
        }
        return unit;
    }

    @Override
    public Long save(Unit entity) throws ServiceException {
        if (entity == null) {
            throw new ServiceException("Unit is null");
        }
        Long id = null;
        try {
            Unit unit = unitRepository.save(entity);
            id = unit != null ? unit.getId() : null;
        } catch (Exception e) {
            throw new ServiceException("Error save unit", e);
        }
        return id;
    }

    @Override
    public Unit find(Long id) throws ServiceException {
        if (id == null) {
            throw new ServiceException("Id is null");
        }
        Unit unit = null;
        try {
            unit = unitRepository.findOne(id);
        } catch (Exception e) {
            throw new ServiceException("Error find unit", e);
        }
        return unit;
    }

    @Override
    public boolean addToUnit(Long unitId, Long employeeId) {
        boolean success = false;
        if (unitId == null || employeeId == null) {
            throw new ServiceException("Unit or employee id is null");
        }
        Unit unit = null;
        Employee employee = null;
        try {
            unit = unitRepository.findOne(unitId);
            employee=employeeRepository.findOne(employeeId);
            if (unit!=null && employee!=null){
                unit.getEmployees().add(employee);
                success=true;
            } else {
                throw new ServiceException("unit or employee not found");
            }

        } catch (Exception e) {
            throw new ServiceException("Error add employee to unit", e);
        }
        return success;
    }
}
