package by.epam.task.service;

import by.epam.task.domain.Employee;

/**
 * Created by Pavel_Mikhailiuk on 11/3/2016.
 */
public interface EmployeeService extends CRUDService<Employee> {
}
