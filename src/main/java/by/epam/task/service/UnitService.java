package by.epam.task.service;

import by.epam.task.domain.Unit;

/**
 * Created by Pavel_Mikhailiuk on 11/3/2016.
 */
public interface UnitService extends CRUDService<Unit> {
    boolean addToUnit(Long unitId, Long employeeId);
}

