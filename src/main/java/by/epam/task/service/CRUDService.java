package by.epam.task.service;

import by.epam.task.service.exception.ServiceException;

public interface CRUDService<T> {

    Long save(T entity) throws ServiceException;

    T find(Long id) throws ServiceException;

    T update(T entity) throws ServiceException;

    void delete(Long id) throws ServiceException;
}
