package by.epam.task.repository;

import by.epam.task.domain.Project;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Pavel_Mikhailiuk on 11/3/2016.
 */
public interface ProjectRepository extends CrudRepository<Project, Long> {
}
