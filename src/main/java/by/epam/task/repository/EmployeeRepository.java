package by.epam.task.repository;

import by.epam.task.domain.Employee;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Pavel_Mikhailiuk on 11/3/2016.
 */
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
}
