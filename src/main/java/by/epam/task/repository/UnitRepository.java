package by.epam.task.repository;

import by.epam.task.domain.Unit;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Pavel_Mikhailiuk on 11/3/2016.
 */
public interface UnitRepository extends CrudRepository<Unit, Long> {
}
