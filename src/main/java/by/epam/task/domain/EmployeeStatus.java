package by.epam.task.domain;

/**
 * Created by Pavel_Mikhailiuk on 11/3/2016.
 */
public enum EmployeeStatus {
    ACTIVE, DISMISSED;
}
