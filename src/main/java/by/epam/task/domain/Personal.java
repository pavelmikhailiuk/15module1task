package by.epam.task.domain;

import javax.persistence.*;

/**
 * Created by Pavel_Mikhailiuk on 11/3/2016.
 */
@Entity
public class Personal {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Integer age;

    @OneToOne
    @PrimaryKeyJoinColumn
    private Employee employee;

    public Personal() {
    }

    public Personal(Integer age) {
        this.age = age;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Personal personal = (Personal) o;

        if (id != null ? !id.equals(personal.id) : personal.id != null) return false;
        return age != null ? age.equals(personal.age) : personal.age == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (age != null ? age.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Personal{" +
                "id=" + id +
                ", age=" + age +
                '}';
    }
}
