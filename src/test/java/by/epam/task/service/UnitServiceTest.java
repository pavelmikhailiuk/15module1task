package by.epam.task.service;

import by.epam.task.domain.Employee;
import by.epam.task.domain.EmployeeStatus;
import by.epam.task.domain.Unit;
import by.epam.task.repository.EmployeeRepository;
import by.epam.task.repository.UnitRepository;
import by.epam.task.service.exception.ServiceException;
import by.epam.task.service.impl.UnitServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UnitServiceTest {

    @Mock
    private UnitRepository unitRepository;

    @Mock
    private EmployeeRepository employeeRepository;

    @InjectMocks
    private UnitServiceImpl unitService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSave() throws ServiceException {
        Unit unit = initUnit();
        unitService.save(unit);
        verify(unitRepository).save(unit);
    }

    @Test
    public void testFind() throws ServiceException {
        Unit expected = initUnit();
        Long id = 1L;
        when(unitService.find(id)).thenReturn(expected);
        Unit actual = unitService.find(id);
        verify(unitRepository).findOne(id);
        assertEquals(expected, actual);
    }

    @Test
    public void testUpdate() throws ServiceException {
        Unit expected = initUnit();
        expected.setId(1L);
        when(unitService.update(expected)).thenReturn(expected);
        Unit actual = unitService.update(expected);
        assertEquals(expected, actual);
    }

    @Test
    public void testDelete() throws ServiceException {
        Long id = 1L;
        unitService.delete(id);
        verify(unitRepository).delete(id);
    }

    @Test
    public void testAddEmployeeToUnit() throws ServiceException {
        Unit unit = initUnit();
        unit.setEmployees(new ArrayList<>());
        Employee employee = initEmployee();
        when(unitRepository.findOne(1L)).thenReturn(unit);
        when(employeeRepository.findOne(1L)).thenReturn(employee);
        assertTrue(unitService.addToUnit(1L, 1L));
    }

    @Test(expected = ServiceException.class)
    public void testAddEmployeeToUnitNullIdException() throws ServiceException {
        unitService.addToUnit(null, null);
    }

    @Test(expected = ServiceException.class)
    public void testAddEmployeeToUnitEmployeeOrUnitNotFoundException() throws ServiceException {
        when(unitRepository.findOne(1L)).thenReturn(null);
        when(employeeRepository.findOne(1L)).thenReturn(null);
        unitService.addToUnit(1L, 1L);
    }

    @Test(expected = RuntimeException.class)
    public void testSaveException() throws ServiceException {
        Unit unit = initUnit();
        when(unitRepository.save(unit)).thenThrow(new RuntimeException());
        unitService.save(unit);
    }

    @Test(expected = RuntimeException.class)
    public void testSaveNullEntityException() throws ServiceException {
        unitService.save(null);
    }

    private Unit initUnit() {
        return new Unit("TR-CBU");
    }

    private Employee initEmployee() {
        return new Employee("Pavel", "Mikhailiuk", EmployeeStatus.ACTIVE);
    }
}
