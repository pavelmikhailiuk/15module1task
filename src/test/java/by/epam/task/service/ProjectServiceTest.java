package by.epam.task.service;

import by.epam.task.domain.Employee;
import by.epam.task.domain.EmployeeStatus;
import by.epam.task.domain.Project;
import by.epam.task.repository.EmployeeRepository;
import by.epam.task.repository.ProjectRepository;
import by.epam.task.service.exception.ServiceException;
import by.epam.task.service.impl.ProjectServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProjectServiceTest {

    @Mock
    private ProjectRepository projectRepository;

    @Mock
    private EmployeeRepository employeeRepository;

    @InjectMocks
    private ProjectServiceImpl projectService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSave() throws ServiceException {
        Project project = initProject();
        projectService.save(project);
        verify(projectRepository).save(project);
    }

    @Test
    public void testFind() throws ServiceException {
        Project expected = initProject();
        Long id = 1L;
        when(projectService.find(id)).thenReturn(expected);
        Project actual = projectService.find(id);
        verify(projectRepository).findOne(id);
        assertEquals(expected, actual);
    }

    @Test
    public void testUpdate() throws ServiceException {
        Project expected = initProject();
        expected.setId(1L);
        when(projectService.update(expected)).thenReturn(expected);
        Project actual = projectService.update(expected);
        assertEquals(expected, actual);
    }

    @Test
    public void testDelete() throws ServiceException {
        Long id = 1L;
        projectService.delete(id);
        verify(projectRepository).delete(id);
    }

    @Test
    public void testAssignEmployeeToProject() throws ServiceException {
        Project project = initProject();
        project.setEmployees(new ArrayList<>());
        Employee employee = initEmployee();
        when(projectRepository.findOne(1L)).thenReturn(project);
        when(employeeRepository.findOne(1L)).thenReturn(employee);
        assertTrue(projectService.assignToProject(1L, 1L));
    }

    @Test(expected = ServiceException.class)
    public void testAssignEmployeeToProjectNullIdException() throws ServiceException {
        projectService.assignToProject(null, null);
    }

    @Test(expected = ServiceException.class)
    public void testAssignEmployeeToProjectEmployeeOrProjectNotFoundException() throws ServiceException {
        when(projectRepository.findOne(1L)).thenReturn(null);
        when(employeeRepository.findOne(1L)).thenReturn(null);
        projectService.assignToProject(1L, 1L);
    }

    @Test(expected = RuntimeException.class)
    public void testSaveException() throws ServiceException {
        Project project = initProject();
        when(projectRepository.save(project)).thenThrow(new RuntimeException());
        projectService.save(project);
    }

    @Test(expected = RuntimeException.class)
    public void testSaveNullEntityException() throws ServiceException {
        projectService.save(null);
    }

    private Project initProject() {
        return new Project("PLNA");
    }

    private Employee initEmployee() {
        return new Employee("Pavel", "Mikhailiuk", EmployeeStatus.ACTIVE);
    }
}
