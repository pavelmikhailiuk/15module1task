package by.epam.task.service;

import by.epam.task.domain.Employee;
import by.epam.task.domain.EmployeeStatus;
import by.epam.task.repository.EmployeeRepository;
import by.epam.task.service.exception.ServiceException;
import by.epam.task.service.impl.EmployeeServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {

    @Mock
    private EmployeeRepository employeeRepository;

    @InjectMocks
    private EmployeeServiceImpl employeeService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSave() throws ServiceException {
        Employee employee = initEmployee();
        employeeService.save(employee);
        verify(employeeRepository).save(employee);
    }

    @Test
    public void testFind() throws ServiceException {
        Employee expected = initEmployee();
        Long id = 1L;
        when(employeeService.find(id)).thenReturn(expected);
        Employee actual = employeeService.find(id);
        verify(employeeRepository).findOne(id);
        assertEquals(expected, actual);
    }

    @Test
    public void testUpdate() throws ServiceException {
        Employee expected = initEmployee();
        expected.setId(1L);
        when(employeeService.update(expected)).thenReturn(expected);
        Employee actual = employeeService.update(expected);
        assertEquals(expected, actual);
    }

    @Test
    public void testDelete() throws ServiceException {
        Long id = 1L;
        employeeService.delete(id);
        verify(employeeRepository).delete(id);
    }

    @Test(expected = RuntimeException.class)
    public void testSaveException() throws ServiceException {
        Employee employee = initEmployee();
        when(employeeRepository.save(employee)).thenThrow(new RuntimeException());
        employeeService.save(employee);
    }

    @Test(expected = RuntimeException.class)
    public void testSaveNullEntityException() throws ServiceException {
        employeeService.save(null);
    }

    private Employee initEmployee() {
        return new Employee("Pavel", "Mikhailiuk", EmployeeStatus.ACTIVE);
    }
}
