package by.epam.task.repository;

import by.epam.task.domain.Project;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Created by Pavel_Mikhailiuk on 11/4/2016.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class ProjectRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProjectRepository projectRepository;

    @Test
    public void findById() throws Exception {
        Project saved = this.entityManager.merge(initProject());
        Project found = this.projectRepository.findOne(saved.getId());
        assertEquals(found, saved);
    }

    @Test
    public void save() throws Exception {
        Project project = initProject();
        Project savedEmployee = this.projectRepository.save(project);
        project.setId(savedEmployee.getId());
        assertEquals(project, savedEmployee);
    }

    @Test
    public void update() throws Exception {
        Project saved = this.entityManager.merge(initProject());
        saved.setName("CPL");
        this.projectRepository.save(saved);
        Project found = projectRepository.findOne(saved.getId());
        assertEquals(found, saved);
    }

    @Test
    public void delete() throws Exception {
        Project saved = this.entityManager.merge(initProject());
        Project found = projectRepository.findOne(saved.getId());
        assertNotNull(found);
        projectRepository.delete(found.getId());
        found = projectRepository.findOne(found.getId());
        assertNull(found);
    }

    private Project initProject() {
        return new Project("PLNA");
    }
}
