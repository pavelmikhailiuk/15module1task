package by.epam.task.repository;

import by.epam.task.domain.Address;
import by.epam.task.domain.Employee;
import by.epam.task.domain.EmployeeStatus;
import by.epam.task.domain.Personal;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Created by Pavel_Mikhailiuk on 11/4/2016.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class EmployeeRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    public void findById() throws Exception {
        Employee saved = this.entityManager.merge(initEmployee());
        Employee found = this.employeeRepository.findOne(saved.getId());
        assertEquals(found, saved);
    }

    @Test
    public void save() throws Exception {
        Employee employee = initEmployee();
        Employee savedEmployee = this.employeeRepository.save(employee);
        employee.setId(savedEmployee.getId());
        assertEquals(employee, savedEmployee);
    }

    @Test
    public void update() throws Exception {
        Employee saved = this.entityManager.merge(initEmployee());
        saved.setName("NewName");
        saved.setSurname("NewSurname");
        saved.setStatus(EmployeeStatus.DISMISSED);
        this.employeeRepository.save(saved);
        Employee found = employeeRepository.findOne(saved.getId());
        assertEquals(found, saved);
    }

    @Test
    public void delete() throws Exception {
        Employee saved = this.entityManager.merge(initEmployee());
        Employee found = employeeRepository.findOne(saved.getId());
        assertNotNull(found);
        employeeRepository.delete(found.getId());
        found = employeeRepository.findOne(found.getId());
        assertNull(found);
    }

    private Employee initEmployee() {
        Employee employee = new Employee("Name", "Surname", EmployeeStatus.ACTIVE);
        Address address = new Address("Minsk", "Radialnaya", 40, 308);
        employee.setAddress(address);
        Personal personal = new Personal(30);
        employee.setPersonal(personal);
        return employee;
    }
}
