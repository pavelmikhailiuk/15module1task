package by.epam.task.repository;

import by.epam.task.domain.Unit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Created by Pavel_Mikhailiuk on 11/4/2016.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class UnitRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UnitRepository unitRepository;

    @Test
    public void findById() throws Exception {
        Unit saved = this.entityManager.merge(initUnit());
        Unit found = this.unitRepository.findOne(saved.getId());
        assertEquals(found, saved);
    }

    @Test
    public void save() throws Exception {
        Unit unit = initUnit();
        Unit savedEmployee = this.unitRepository.save(unit);
        unit.setId(savedEmployee.getId());
        assertEquals(unit, savedEmployee);
    }

    @Test
    public void update() throws Exception {
        Unit saved = this.entityManager.merge(initUnit());
        saved.setName("CPL");
        this.unitRepository.save(saved);
        Unit found = unitRepository.findOne(saved.getId());
        assertEquals(found, saved);
    }

    @Test
    public void delete() throws Exception {
        Unit saved = this.entityManager.merge(initUnit());
        Unit found = unitRepository.findOne(saved.getId());
        assertNotNull(found);
        unitRepository.delete(found.getId());
        found = unitRepository.findOne(found.getId());
        assertNull(found);
    }

    private Unit initUnit() {
        return new Unit("TR-CBU");
    }
}
